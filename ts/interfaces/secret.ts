import * as interfaces from './index.js';

export interface ISecretCreationDescriptor {
  name: string;
  version: string;
  contentArg: any;
  labels: interfaces.TLabels;
}
