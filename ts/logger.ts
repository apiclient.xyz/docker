import * as plugins from './plugins.js';
import { commitinfo } from './00_commitinfo_data.js';

export const logger = plugins.smartlog.Smartlog.createForCommitinfo(commitinfo);
logger.enableConsole();