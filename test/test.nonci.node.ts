import { expect, tap } from '@push.rocks/tapbundle';
import { Qenv } from '@push.rocks/qenv';

const testQenv = new Qenv('./', './.nogit/');

import * as plugins from '../ts/plugins.js';
import * as paths from '../ts/paths.js';

import * as docker from '../ts/index.js';

let testDockerHost: docker.DockerHost;

tap.test('should create a new Dockersock instance', async () => {
  testDockerHost = new docker.DockerHost({});
  await testDockerHost.start();
  return expect(testDockerHost).toBeInstanceOf(docker.DockerHost);
});

tap.test('should create a docker swarm', async () => {
  await testDockerHost.activateSwarm();
});

// Containers
tap.test('should list containers', async () => {
  const containers = await testDockerHost.getContainers();
  console.log(containers);
});

// Networks
tap.test('should list networks', async () => {
  const networks = await testDockerHost.getNetworks();
  console.log(networks);
});

tap.test('should create a network', async () => {
  const newNetwork = await docker.DockerNetwork.createNetwork(testDockerHost, {
    Name: 'webgateway',
  });
  expect(newNetwork).toBeInstanceOf(docker.DockerNetwork);
  expect(newNetwork.Name).toEqual('webgateway');
});

tap.test('should remove a network', async () => {
  const webgateway = await docker.DockerNetwork.getNetworkByName(testDockerHost, 'webgateway');
  await webgateway.remove();
});

// Images
tap.test('should pull an image from imagetag', async () => {
  const image = await docker.DockerImage.createFromRegistry(testDockerHost, {
    creationObject: {
      imageUrl: 'hosttoday/ht-docker-node',
      imageTag: 'alpine',
    },
  });
  expect(image).toBeInstanceOf(docker.DockerImage);
  console.log(image);
});

tap.test('should return a change Observable', async (tools) => {
  const testObservable = await testDockerHost.getEventObservable();
  const subscription = testObservable.subscribe((changeObject) => {
    console.log(changeObject);
  });
  await tools.delayFor(2000);
  subscription.unsubscribe();
});

// SECRETS
tap.test('should create a secret', async () => {
  const mySecret = await docker.DockerSecret.createSecret(testDockerHost, {
    name: 'testSecret',
    version: '1.0.3',
    contentArg: `{ "hi": "wow"}`,
    labels: {},
  });
  console.log(mySecret);
});

tap.test('should remove a secret by name', async () => {
  const mySecret = await docker.DockerSecret.getSecretByName(testDockerHost, 'testSecret');
  await mySecret.remove();
});

// SERVICES
tap.test('should activate swarm mode', async () => {
  await testDockerHost.activateSwarm();
});

tap.test('should list all services', async (tools) => {
  const services = await testDockerHost.getServices();
  console.log(services);
});

tap.test('should create a service', async () => {
  const testNetwork = await docker.DockerNetwork.createNetwork(testDockerHost, {
    Name: 'testNetwork',
  });
  const testSecret = await docker.DockerSecret.createSecret(testDockerHost, {
    name: 'testSecret',
    version: '0.0.1',
    labels: {},
    contentArg: '{"hi": "wow"}',
  });
  const testImage = await docker.DockerImage.createFromRegistry(testDockerHost, {
    creationObject: {
      imageUrl: 'code.foss.global/host.today/ht-docker-node:latest',
    }
  });
  const testService = await docker.DockerService.createService(testDockerHost, {
    image: testImage,
    labels: {},
    name: 'testService',
    networks: [testNetwork],
    networkAlias: 'testService',
    secrets: [testSecret],
    ports: ['3000:80'],
  });

  await testService.remove();
  await testNetwork.remove();
  await testSecret.remove();
});

tap.test('should export images', async (toolsArg) => {
  const done = toolsArg.defer();
  const testImage = await docker.DockerImage.createFromRegistry(testDockerHost, {
    creationObject: {
      imageUrl: 'code.foss.global/host.today/ht-docker-node:latest',
    }
  });
  const fsWriteStream = plugins.smartfile.fsStream.createWriteStream(
    plugins.path.join(paths.nogitDir, 'testimage.tar')
  );
  const exportStream = await testImage.exportToTarStream();
  exportStream.pipe(fsWriteStream).on('finish', () => {
    done.resolve();
  });
  await done.promise;
});

tap.test('should import images', async (toolsArg) => {
  const done = toolsArg.defer();
  const fsReadStream = plugins.smartfile.fsStream.createReadStream(
    plugins.path.join(paths.nogitDir, 'testimage.tar')
  );
  await docker.DockerImage.createFromTarStream(testDockerHost, {
    tarStream: fsReadStream,
    creationObject: {
      imageUrl: 'code.foss.global/host.today/ht-docker-node:latest',
    }
  })
});

tap.test('should expose a working DockerImageStore', async () => {
  // lets first add am s3 target
  const s3Descriptor = {
    endpoint: await testQenv.getEnvVarOnDemand('S3_ENDPOINT'),
    accessKey: await testQenv.getEnvVarOnDemand('S3_ACCESSKEY'),
    accessSecret: await testQenv.getEnvVarOnDemand('S3_ACCESSSECRET'),
    bucketName: await testQenv.getEnvVarOnDemand('S3_BUCKET'),
  };
  await testDockerHost.addS3Storage(s3Descriptor);

  //
  await testDockerHost.imageStore.storeImage('hello', plugins.smartfile.fsStream.createReadStream(plugins.path.join(paths.nogitDir, 'testimage.tar')));
})

export default tap.start();
